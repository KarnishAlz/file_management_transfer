﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FileManagementAppWindowsV1
{
    public partial class FileManager : Form
    {
       
        Home homeManager = new Home();
        ViewFileManagement viewFileManagement = new ViewFileManagement();
        public FileManager()
        {
            InitializeComponent();
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            
            if (WorkerTextBox.Text!= string.Empty) 
            {
                workerErrorProvider.Clear();
                if(TaskTextBox.Text== string.Empty)
                {
                    TaskTextBox.Focus();
                    taskErrorProvider.SetError(label2, "Required");
                }
                else
                {
                    taskErrorProvider.Clear();
                    if (TaskDetailTextBox.Text==string.Empty)
                    {
                        TaskDetailTextBox.Focus();
                        taskDetailErrorProvider.SetError(label3, "Required");

                    }
                    else
                    {
                        taskDetailErrorProvider.Clear();
                        int n = dataGridView1.Rows.Add();
                        dataGridView1.Rows[n].Cells[0].Value = TaskTextBox.Text;
                        dataGridView1.Rows[n].Cells[1].Value = TaskDetailTextBox.Text;
                        ErrorLabel.Text = "";

                    }

                }
               

            }
            else
            {
                WorkerTextBox.Focus();
                workerErrorProvider.SetError(label1, "Required");
            }
           
            

           
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            int rowCount = dataGridView1.Rows.Count;

            if (rowCount!=0 )
            {
                DataSet dataSet = new DataSet();

                DataTable dataTableWorker = new DataTable();
                dataTableWorker.TableName = "Workers";
                dataTableWorker.Columns.Add("WorkerName");
                dataSet.Tables.Add(dataTableWorker);


                DataTable dataTableTask = new DataTable();
                dataTableTask.TableName = "Tasks";
                dataTableTask.Columns.Add("TaskName");
                dataTableTask.Columns.Add("TaskDetail");
                dataSet.Tables.Add(dataTableTask);



                DataRow dataRow = dataSet.Tables["Workers"].NewRow();
                dataRow["WorkerName"] = WorkerTextBox.Text;
                dataSet.Tables["Workers"].Rows.Add(dataRow);


                foreach (DataGridViewRow r in dataGridView1.Rows)
                {
                    DataRow dataRow1 = dataSet.Tables["Tasks"].NewRow();
                    dataRow1["TaskName"] = r.Cells[0].Value.ToString();
                    dataRow1["TaskDetail"] = r.Cells[1].Value.ToString();
                    dataSet.Tables["Tasks"].Rows.Add(dataRow1);

                }
                dataSet.WriteXml("E:\\Tasks\\Data_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml");

                dataGridView1.Rows.Clear();
                TaskTextBox.Clear();
                TaskDetailTextBox.Clear();
                WorkerTextBox.Clear();
                ErrorLabel.Text = "";

            }

            else

            {
                ErrorLabel.Text = "Please input some data";
                ErrorLabel.Focus();
            }

           
        }

        private void LoadButton_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            DataSet dataSet = new DataSet();
            var directory =new DirectoryInfo("E:\\Tasks");
            string myFile = directory.GetFiles().OrderByDescending(f => f.LastWriteTime).First().ToString();
            dataSet.ReadXml("E:\\Tasks\\" + myFile);
            WorkerTextBox.Text = dataSet.Tables["Workers"].Rows[0][0].ToString();
           
            foreach(DataRow item in dataSet.Tables["Tasks"].Rows)
            {
                
                int n = dataGridView1.Rows.Add();
                dataGridView1.Rows[n].Cells[0].Value = item["TaskName"].ToString();
                dataGridView1.Rows[n].Cells[1].Value = item["TaskDetail"].ToString();


            }

        }

       

        private void EditButton_Click(object sender, EventArgs e)
        {
            dataGridView1.SelectedRows[0].Cells[0].Value = TaskTextBox.Text;
            dataGridView1.SelectedRows[0].Cells[1].Value = TaskDetailTextBox.Text;
        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.RemoveAt(dataGridView1.SelectedRows[0].Index);
        }

       

       

        private void ViewButton_Click(object sender, EventArgs e)
        {
            this.Hide();

            viewFileManagement.Closed += (s, args) => this.Close();

            viewFileManagement.Show();


        }



        private void dataGridView1_MouseClick_1(object sender, MouseEventArgs e)
        {
            TaskTextBox.Text = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
            TaskDetailTextBox.Text = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();

        }

        private void HomeButton_Click_1(object sender, EventArgs e)
        {
            this.Hide();
            homeManager.Closed += (s, args) => this.Close();
            homeManager.Show();
        }
    }
}
