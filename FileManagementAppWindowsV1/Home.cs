﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FileManagementAppWindowsV1
{
    public partial class Home : Form
    {
       
        public Home()
        {
            InitializeComponent();
        }

        private void createButton_Click(object sender, EventArgs e)
        {

            FileManager fileManager = new FileManager();
            this.Hide();
            fileManager.Closed += (s, args) => this.Close();
            fileManager.Show();

        }

        private void viewButton_Click(object sender, EventArgs e)
        {
            ViewFileManagement viewFileManagement = new ViewFileManagement();
            this.Hide();

            viewFileManagement.Closed += (s, args) => this.Close();
           
            viewFileManagement.Show();

        }
    }
}
