﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FileManagementAppWindowsV1
{
    public partial class ViewFileManagement : Form
    {
        public ViewFileManagement()
        {
            InitializeComponent();
        }
        Home homeManager = new Home();
        private void LoadFilesutton_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            var directory = new DirectoryInfo("E:\\Tasks\\");
            DateTime from_Date = FromDateTimePicker.Value;
            DateTime to_Date = ToDateTimePicker.Value;
            var files = directory.GetFiles().Where(x => x.CreationTime.Date>=from_Date && x.CreationTime.Date<=to_Date);
            foreach(FileInfo f in files)
            {
                int n=dataGridView1.Rows.Add();
                dataGridView1.Rows[n].Cells[0].Value = f.Name;
                dataGridView1.Rows[n].Cells[1].Value = f.CreationTime;
            }
           
        }

        private void createButton_Click(object sender, EventArgs e)
        {
            FileManager fileManager = new FileManager();
            this.Hide();
            fileManager.Closed += (s, args) => this.Close();
            fileManager.Show();

        }

      
      

        private void HomeButton_Click_1(object sender, EventArgs e)
        {
            Home homeManager = new Home();
            this.Hide();
            homeManager.Closed += (s, args) => this.Close();
            homeManager.Show();
        }
    }
}
