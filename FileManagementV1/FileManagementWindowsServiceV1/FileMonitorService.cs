﻿

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Net.NetworkInformation;
using System.Net.Http.Headers;
using FileManagementWindowsServiceV1.Model;

namespace FileManagementWindowsServiceV1
{
    public partial class FileMonitorService : ServiceBase
    {
        Timer timer1 = new Timer();
        Timer timer2 = new Timer();




        public FileMonitorService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {


             timer1.Elapsed += new ElapsedEventHandler(OnElapsedTimePost);
            timer1.Interval = 60000 * 1;
            timer1.Enabled = true;
            // NetworkChange.NetworkAvailabilityChanged += AvailabilityChanged;
            timer2.Elapsed += new ElapsedEventHandler(OnElapsedTimeGet);
            timer2.Interval = 60000 * 3;
            timer2.Enabled = true;

        }
        public List<FileEntry> hasNewFiles()
        {

            List<FileEntry> fileEntries = new List<FileEntry>();
            using (ClientDBEntities dBEntitiesV1 = new ClientDBEntities())
            {
                FileEntry _fileEntry = new FileEntry();
                fileEntries = dBEntitiesV1.FileEntries.Where(a => a.Status == 0).ToList();
            }
            return fileEntries;
        }

        /* private async void AvailabilityChanged(object sender,NetworkAvailabilityEventArgs e)
         {
             if (e.IsAvailable)
             {
                 try
                 {
                     using (var client = new HttpClient())
                     {
                         client.BaseAddress = new Uri("https://localhost:44320/");
                         client.DefaultRequestHeaders.Accept.Clear();
                         client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                         GetApi(client).Wait();
                     }
                 }

                 catch (AggregateException ae)
                 {
                     throw ae;
                 }

             }

         }*/

        public void PostFile(FileEntry fileEntry)
        {
            var fileModel = new FileModel()
            {
                FileBody = File.ReadAllBytes(fileEntry.FilePath),
                Client = fileEntry.Client,
                Created = fileEntry.Created,
                FileName = fileEntry.FileName,
                Id = fileEntry.Id,
                Sent = DateTime.Now,
                Status = 0
            };
            using (var client = new HttpClient())
            {


                client.BaseAddress = new Uri("http://localhost:55555/api/");
                var postTask = client.PostAsJsonAsync<FileModel>("FileEntries", fileModel);
                postTask.Wait();
                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    UpdateStatus(fileEntry.FileName);
                }
                else
                {
                    Console.WriteLine(result.StatusCode);
                }

            }
        }

        private void OnElapsedTimePost(object sender, ElapsedEventArgs e)
        {

            List<FileEntry> _filelist = hasNewFiles();

            foreach (FileEntry i in _filelist)
            {

                using (ClientDBEntities entities = new ClientDBEntities())
                {
                    PostFile(i);

                }
            }


        }
        public void UpdateStatus(string fileName)
        {
            using (ClientDBEntities entities = new ClientDBEntities())
            {
                var result = entities.FileEntries.Where(p => p.FileName == fileName).Single();
                result.Status = 1;
                entities.SaveChanges();
            }
        }

        protected override void OnStop()
        {
            EventLog.WriteEntry("Directory Montitor service STOPED");
        }



        private void OnElapsedTimeGet(object sender, ElapsedEventArgs e)
        {
            GetFile();
        }
        public void GetFile()
        {
            using (var client = new HttpClient())
            {
                var pcClient = System.Environment.MachineName;
                client.BaseAddress = new Uri("http://localhost:55555/api/");
                var responseTask = client.GetAsync("Download/GetFile/" + pcClient);
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<FileModel>();
                    readTask.Wait();
                    var insertFileModel = readTask.Result;
                   if( SaveFile(insertFileModel)!=false)
                    {
                        if(PutStatus(insertFileModel.Id)==false)
                        {
                            Console.WriteLine("Failed");
                        }

                    }
                }
                else
                {
                    Console.WriteLine(result.StatusCode);
                }

            }
        }
       static bool SaveFile(FileModel fileModel)
        {
            AddToDirectory(fileModel.FileBody,fileModel.FileName);
            using (ClientDBEntities entities = new ClientDBEntities())
            {
                if (!entities.FileEntries.Any(e => e.FileName == fileModel.FileName))
                {
                    FileEntry fileEntry = new FileEntry();
                    fileEntry.Client = fileModel.Client;
                    fileEntry.Created = fileModel.Created;
                    fileEntry.Sent = DateTime.Now;
                    fileEntry.Status = 1;
                    fileEntry.FilePath = "E:\\server\\" + fileModel.FileName;
                    fileEntry.FileName = fileModel.FileName;
                    entities.FileEntries.Add(fileEntry);
                    entities.SaveChanges();
                    

                }
                return true;

            }
          
        }
        static void AddToDirectory(byte[] file,string name)
        {
            if (!Directory.Exists("E:\\server\\"))
            {
                Directory.CreateDirectory("E:\\server\\");
            }
            if (!Directory.Exists("E:\\server\\" + name))
            {
                using (FileStream fileStream = File.Create("E:\\server\\" + name))
                {
                    fileStream.Write(file, 0, file.Length);
                    fileStream.Flush();

                }
            }
        }
        public bool PutStatus(int id)
        {
            using (var client = new HttpClient())
            {


                client.BaseAddress = new Uri("http://localhost:55555/api/");
                var postTask = client.GetAsync("Download/GetStatus/" + id);
                postTask.Wait();
                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }
    }
}
