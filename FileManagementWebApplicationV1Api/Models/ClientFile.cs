﻿using System;
using System.Collections.Generic;

namespace FileManagerWebApplicationV1
{
    public partial class ClientFile
    {
        public int Id { get; set; }
        public int FileId { get; set; }
        public string Client { get; set; }
        public int Status { get; set; }
    }
}
