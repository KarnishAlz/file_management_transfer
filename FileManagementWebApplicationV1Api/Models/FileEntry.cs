﻿using System;
using System.Collections.Generic;

namespace FileManagerWebApplicationV1
{
    public partial class FileEntry
    {
        public int Id { get; set; }
        public string FileName { get; set; }
        public DateTime Created { get; set; }
        public DateTime Sent { get; set; }
        public int Status { get; set; }
        public string Client { get; set; }
        public string Path { get; set; }
    }
}
