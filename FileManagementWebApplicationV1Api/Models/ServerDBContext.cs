﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace FileManagerWebApplicationV1
{
    public partial class ServerDBContext : DbContext
    {
        public ServerDBContext()
        {
        }

        public ServerDBContext(DbContextOptions<ServerDBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Client> Client { get; set; }
        public virtual DbSet<ClientFile> ClientFile { get; set; }
        public virtual DbSet<FileEntry> FileEntry { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=DESKTOP-6KNP71K;Database=ServerDB;User Id=sa;password=123456789;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Client>(entity =>
            {
                entity.Property(e => e.Client1)
                    .IsRequired()
                    .HasColumnName("Client")
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ClientFile>(entity =>
            {
                entity.Property(e => e.Client)
                    .IsRequired()
                    .IsUnicode(false);
            });

            modelBuilder.Entity<FileEntry>(entity =>
            {
                entity.Property(e => e.Client)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.Created).HasColumnType("date");

                entity.Property(e => e.FileName)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.Path)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.Sent).HasColumnType("date");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
