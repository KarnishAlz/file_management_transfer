﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using FileManagerWebApplicationV1.DemoModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FileManagerWebApplicationV1.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class DownloadController : ControllerBase
    {
        [HttpGet("{clientName}")]
        public async Task<ActionResult<FileModel>> GetFile(string clientName)
        {
            FileModel fileModel = new FileModel();
            using (var _context = new ServerDBContext())
            {
                ClientFile clientFile = new ClientFile();
                FileEntry fileEntry = new FileEntry();
                clientFile = await ClientExists(clientName);
                int id = clientFile.FileId;
                fileEntry = await FileExists(id);
                fileModel.Client = clientFile.Client;
                fileModel.Created = fileEntry.Created;
                fileModel.FileName = fileEntry.FileName;
                fileModel.FileBody = System.IO.File.ReadAllBytes(fileEntry.Path);
                fileModel.Sent = DateTime.Now;
                fileModel.Status = 0;
                fileModel.Id = fileEntry.Id;

            }
            return fileModel;
        }


        [HttpGet("{id}")]
        public async Task<HttpResponseMessage> GetStatus(int id)
        {
            if (await UpdateDb(id) != false)
            {

                return new HttpResponseMessage(HttpStatusCode.OK);


            }
            return null;
        }
            public async Task<bool> UpdateDb(int id)

        {
            using (var _context = new ServerDBContext())
            {
                FileEntry fileEntry=await _context.FileEntry.FindAsync(id);
                var clientFile =await _context.ClientFile.Where(e => e.FileId == id && e.Client == fileEntry.Client).FirstAsync();

                clientFile.Status = 1;

                try
                {
                    await _context.SaveChangesAsync();
                }
                catch
                {
                    return false;
                }
            }

            return true;
        }
   

            public async Task<ClientFile> ClientExists(string client)
        {
            using (var _context = new ServerDBContext())
            {
                var clientFile =_context.ClientFile.Where(e => e.Client != client && e.Status == 0).FirstOrDefault();
                return clientFile;
            }
        }
        public async Task<FileEntry> FileExists(int id)
        {
            using (var _context = new ServerDBContext())
            {
               var fileEntry = _context.FileEntry.Where(e => e.Id == id).FirstOrDefault();
                return fileEntry;
            }
        }
    }

    }
