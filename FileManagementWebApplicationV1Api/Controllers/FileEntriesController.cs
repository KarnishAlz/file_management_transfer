﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FileManagerWebApplicationV1.DemoModel;
using System.Net;
using System.IO;
using System.Net.Http;
using Microsoft.AspNetCore.Hosting;

namespace FileManagerWebApplicationV1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class FileEntriesController : ControllerBase
    {
        public static IWebHostEnvironment _env;
        public FileEntriesController (IWebHostEnvironment environment)
        {
            _env = environment;
        }

        [HttpPost]
        [DisableRequestSizeLimit]
        public async Task<HttpResponseMessage> PostFileEntryAsync(FileModel fileModel)
        {
            if (await UploadFileToFTP(fileModel) != false)
            {
                if (await InsertDataFileEntryAsync(fileModel) != false)
                {
                    if (await UpdateDb(fileModel.FileName) == true)
                    {
                        return new HttpResponseMessage(HttpStatusCode.OK);

                    }

                }

            }
            return null;
        }
        public async Task<bool> InsertDataFileEntryAsync(FileModel fileModel)
        {

            using (var ctx = new ServerDBContext())
            {
                FileEntry fileEntry = new FileEntry();

                Client client = new Client();
                if (FileExists(fileModel.FileName) != true)
                {
                    fileEntry.FileName = fileModel.FileName;
                    fileEntry.Client = fileModel.Client;
                    fileEntry.Created = fileModel.Created;
                    fileEntry.Sent = fileModel.Sent;
                    fileEntry.Status = 0;
                    fileEntry.Path = _env.WebRootPath + "\\UploadedFiles\\" + fileModel.FileName;

                    ctx.FileEntry.Add(fileEntry);
                }
                if (ClientExists(fileModel.Client) != true)
                {
                    client.Client1 = fileModel.Client;
                    ctx.Client.Add(client);

                }

                try
                {
                    await ctx.SaveChangesAsync();
                }
                catch (DbUpdateException)
                {

                    return false;
                }

            }

            return true;


        }
        public async Task<bool> UpdateDb(string fileName)

        {
            ClientFile clientFile = new ClientFile();
            List<Client> listOfClients = new List<Client>();
            //List<FileEntry> listOfFileEntry = new List<FileEntry>();

            try
            {
                using (var _context = new ServerDBContext())
                {
                    listOfClients = _context.Client.ToList();
                    var fileEntry = _context.FileEntry.Where(e => e.FileName == fileName).FirstOrDefault();

                    foreach (var i in listOfClients)
                    {
                       
                        if(clientFilexists(i.Client1,fileEntry.Id)!=true)
                        {
                            clientFile.Client = i.Client1;
                            clientFile.FileId = fileEntry.Id;
                            clientFile.Status = 0;
                            _context.ClientFile.Add(clientFile);
                            try
                            {
                                await _context.SaveChangesAsync();
                            }
                            catch
                            {
                                return false;
                            }
                        }
                        
                      
                    }

                    return true;
                }
            }
            catch (DbUpdateException)
            {
                return false;
            }


        }

        public bool ClientExists(string client)
        {
            using (var _context = new ServerDBContext())
            {
                return _context.Client.Any(e => e.Client1 == client);
            }
        }
        public bool FileExists(string file)
        {
            using (var _context = new ServerDBContext())
            {
                return _context.FileEntry.Any(e => e.FileName == file);
            }
        }
        public bool clientFilexists(string client,int fileId)
        {
            using (var _context = new ServerDBContext())
            {
                return _context.ClientFile.Any(e => e.Client == client && e.FileId==fileId);
            }
        }


        //File Upload
        /*  private static bool UploadFileToFTP(FileModel fileEntry)

            {
                Uri ftpurl = new Uri("ftp://ftp.connectingseafarer.com");
                // e.g.ftp://serverip/foldername/foldername
                // String ftpusername = "@remote@marinoft.com"; // e.g. username
                //String ftppassword = "@Yn~!&IgTLoO("; // e.g. password
                try
                {
                    string filename = fileEntry.FileName;
                    //string ftpfullpath = ftpurl;
                    FtpWebRequest ftp = (FtpWebRequest)FtpWebRequest.Create(ftpurl + @"/" + filename);
                    ftp.Credentials = new NetworkCredential("remote@marinoft.com", "Yn~!&IgTLoO(");

                    ftp.KeepAlive = true;
                    ftp.UseBinary = true;
                    ftp.Method = WebRequestMethods.Ftp.UploadFile;

                    //FileStream fs = System.IO.File.OpenRead(fileEntry.FilePath);
                    byte[] buffer = new byte[fileEntry.FileBody.Length];
                    // fs.Read(buffer, 0, buffer.Length);
                    // fs.Close();

                    Stream ftpstream = ftp.GetRequestStream();
                    ftpstream.Write(buffer, 0, buffer.Length);
                    ftpstream.Close();
                    return true;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            */

        private async Task<bool> UploadFileToFTP(FileModel fileEntry)

        {

            if (fileEntry.FileBody.Length > 0)
            {
                if (!Directory.Exists(_env.WebRootPath + "\\UploadedFiles\\"))
                {
                    Directory.CreateDirectory(_env.WebRootPath + "\\UploadedFiles\\");
                }
                if(!Directory.Exists(_env.WebRootPath + "\\UploadedFiles\\" + fileEntry.FileName))
                {
                    using (FileStream fileStream = System.IO.File.Create(_env.WebRootPath + "\\UploadedFiles\\" + fileEntry.FileName))
                    {
                        fileStream.Write(fileEntry.FileBody, 0, fileEntry.FileBody.Length);
                        fileStream.Flush();
                        return true;
                    }

                }
               

            }
            return false;

        }
    }
}
