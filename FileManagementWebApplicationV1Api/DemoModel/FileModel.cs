﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FileManagerWebApplicationV1.DemoModel
{
    public class FileModel
    {
        public int Id { get; set; }
        public string FileName { get; set; }
        public System.DateTime Created { get; set; }
        public System.DateTime Sent { get; set; }
        public int Status { get; set; }

        public string Client { get; set; }
        public byte[] FileBody
        {
            get; set;
        }
    }
}
